class RakeTaskMailer < ActionMailer::Base
  default :from => "info@gurujiforpuja.com"
  default :to => "director@truespider.com"
  
  self.delivery_method = :active_record
  
  def notify(subj, mesg)
    mail(:subject => subj, :body => mesg)
  end
  
  def notify_error(subj, err)
    mail(:subject => subj, :body => err.backtrace.join('\n'))
  end
end
