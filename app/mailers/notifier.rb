class Notifier < ActionMailer::Base
  default from: "info@gurujiforpuja.com"
  self.delivery_method = :active_record
  
  
  def notify_guruji_signup(guruji)
  	@guruji = guruji
  	mail(:to => NOTIFIER_RECEIVERS, :subject => "New Guruji Signed Up!")
  end
  
  
  def booking_confirmation_to_admin(booking)
  	@booking = booking
  	mail(:to => NOTIFIER_RECEIVERS, :subject => "New Booking on GurujiForPuja!")
  end
  
  
  def booking_confirmation_to_customer(booking)
  	@booking = booking
  	mail(:to => booking.customer.email, :subject => "Booking confirmed on GurujiForPuja!")
  end
end
