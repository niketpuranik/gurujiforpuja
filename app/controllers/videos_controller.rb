class VideosController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
  
  layout "dashboard"
  
  # GET /videos
  # GET /videos.json
  def index
    @videos = Video.all 

    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json { render json: @videos }
    end
  end

  # GET /videos/1
  # GET /videos/1.json
  def show
    @video = Video.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js
      format.json { render json: @video }
    end
  end

  # GET /videos/new
  # GET /videos/new.json
  def new
    @video = Video.new

    respond_to do |format|
      format.html { render :layout => false } # new.html.erb
      format.js
      format.json { render json: @video }
    end
    
  end

  # GET /videos/1/edit
  def edit
    @video = Video.find(params[:id])
     respond_to do |format|
      format.html # new.html.erb
      format.js
      format.json { render json: @video }
    end
  end

  # POST /videos
  # POST /videos.json
  def create
    @video = Video.new(params[:video])
    @success = @video.save
    
    respond_to do |format|
      if @success
        format.html { redirect_to @video, notice: 'Video was successfully created.' }
        format.js
        format.json { render json: @video, status: :created, location: @video }
      else
        format.html { render action: "new" }
        format.js
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /videos/1
  # PUT /videos/1.json
  def update
    @video = Video.find(params[:id])

    respond_to do |format|
      if @video.update_attributes(params[:video])
        format.html { redirect_to @video, notice: 'Video was successfully updated.' }
        format.js
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.js
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video = Video.find(params[:id])
    session[:deleted_video_id] = params[:id]
    @video.destroy

    respond_to do |format|
      format.html { redirect_to videos_url }
      format.js
      format.json { head :no_content }
    end
  end

end
