class SiteController < ApplicationController

	def index
		#@top_guruji = Guruji.joins(:subscriptions => :plan).where("plans.id = ?",Plan.order("price").first.id)
		@top_guruji = Guruji.all(:order => "priority")
		@contact = Contact.new
	end

	def services

	end 
	
	def tooltips
		render :layout => false
	end

	def pooja_vidhi
		@pujas = Puja.metasearch(params[:search]).all
	end

	def astrology

	end

	def calendar

	end

	def plan
		@plans = Plan.all
	end

	def guruji
		@guruji = Guruji.find(params[:guruji_id]) rescue nil
		if @guruji
			@guruji.increment_profile_views!
		else
			redirect_to(root_url)
		end
	end

	def search

=begin
		if params[:location_id].present? and params[:location_id].to_i == 0
			loc = Location.where(:city_id => params[:city_id]).where("name like ?","%#{params[:location_id]}%").first
			if not loc
				loc = Location.new(:name => params[:location_id], :city_id => params[:city_id])
				loc.save
				params[:location_id] = loc.id
			else
				params[:location_id] = loc.id
			end
		end
=end

		#@chk_puja_name = Puja.where(:id => params[:puja_id]).first.name
		@chk_puja_name = params[:puja_id]
		if SERVICES.include? @chk_puja_name
			@test_string = "Servise"
			@service_name = SERVICES[@chk_puja_name]
			get_guruji_for_servise();

		else
			@test_string = "Puja"
			get_guruji_for_puja();
		end
		
		respond_to do |format|
		format.html
		end
	end

	def get_locations
		@locations = Location.where(:city_id => params[:city_id])
		@location_select_id = params[:location_select_id]
		@location_id = params[:location_id]
		respond_to do |format|
			format.js
		end
	end

	def contact_us
			if request.xhr?
				@contact = Contact.new(params[:contact])
				@success = @contact.save	
			
				respond_to do |format|
					format.js
				end
			else
				@contact = Contact.new
				respond_to do |format|
					format.html
				end
			end
	end
	
	def terms
	end

	def privacy_policy
	end
	
	def get_guruji_for_servise
		@search_type = params[:puja_id]
		session[:puja_id] = params[:puja_id]
		session[:location_id] = params[:location_id]
		session[:city_id] = params[:city_id]

		#@puja = Puja.where(:id => params[:puja_id]).first.name
		@puja = Puja.where(:name => params[:puja_id]).first
		puja_name = @puja.name rescue nil
		session[:puja] = puja_name
		session[:search_string] = "Search results for #{puja_name}"

		@puja ||= Puja.new
		
		@events = DailyEvent.where("from_time > ?", Time.now).page(params[:page]).per(10)

		@guruji_ids = []
		
		Plan.order("price DESC").all.each_with_index do |plan,index|

			scope = Guruji.where("locations like ?","%#{params[:location_id]}%").where("offered_services like ?","%#{@service_name}%").joins(:subscriptions => :plan).where("plans.id = ?",plan.id)
		
			non_visited = scope.where("users.visited = ?",false)
		
			if non_visited.count == 0
				scope.update_all(:visited => false)
			end
			limit  = PLANS_SEARCH_COUNT[plan.price]
			@guruji_ids << non_visited.limit(limit).select("users.id").collect{|x| x.id}
		end
		
		@guruji_ids.flatten!
		@final_ids = @guruji_ids
		@gurujis = Guruji.readonly(false).where(:id => @final_ids).joins(:subscriptions => :plan).order("plans.price DESC")
		@gurujis.each{|x| x.increment_search_views!}
		
		SearchCriteria.delay.create!(:offered_services => @service_name, :location_id => params[:location_id], :city_id => params[:city_id], :guruji_ids => @final_ids.join("|"), :session_id => current_session_id, :user_id => current_user_id)
		
		
		if params[:city_id].present?
			city_name = City.where(:id => params[:city_id]).first.name rescue nil
			session[:city] = city_name
			session[:search_string] +=  " in #{city_name}"
		else
			session[:city] = nil
			session[:city_id] = nil
			session[:location] = nil
			session[:location_id] = nil
		end
		
		if params[:location_id].present?
			location_name = Location.where(:id => params[:location_id]).first.name rescue nil
			session[:location] = location_name
			session[:search_string] += " near #{location_name}"
		else
			session[:location] = nil
			session[:location_id] = nil
		end
	end


	def get_guruji_for_puja
		@search_type = params[:puja_id]
		session[:puja_id] = params[:puja_id]
		session[:location_id] = params[:location_id]
		session[:city_id] = params[:city_id]

		if params[:puja_id].present?
			@puja = Puja.where(:id => params[:puja_id]).first
			puja_name = @puja.name rescue nil
			session[:puja] = puja_name
			session[:search_string] = "Search results for #{puja_name}"
		else
			session[:puja] = nil
		end

		@puja ||= Puja.new
		
		@events = DailyEvent.where("from_time > ?", Time.now).where("puja_ids like ?","%#{@puja.id}%").page(params[:page]).per(10)
		
		@guruji_ids = []
		
		Plan.order("price DESC").all.each_with_index do |plan,index|
			scope = Guruji.where("locations like ?","%#{params[:location_id]}%").where("offered_pujas like ?","%#{@puja.id}%").joins(:subscriptions => :plan).where("plans.id = ?",plan.id)
		
			non_visited = scope.where("users.visited = ?",false)
		
			if non_visited.count == 0
				scope.update_all(:visited => false)
			end
			limit  = PLANS_SEARCH_COUNT[plan.price]
			@guruji_ids << non_visited.limit(limit).select("users.id").collect{|x| x.id}
		end
		
		@guruji_ids.flatten!
		@final_ids = @guruji_ids
		@gurujis = Guruji.readonly(false).where(:id => @final_ids).joins(:subscriptions => :plan).order("plans.price DESC")
		@gurujis.each{|x| x.increment_search_views!}
		
		SearchCriteria.delay.create!(:puja_id => @puja.id, :location_id => params[:location_id], :city_id => params[:city_id], :guruji_ids => @final_ids.join("|"), :session_id => current_session_id, :user_id => current_user_id)
		
		
		if params[:city_id].present?
			city_name = City.where(:id => params[:city_id]).first.name rescue nil
			session[:city] = city_name
			session[:search_string] +=  " in #{city_name}"
		else
			session[:city] = nil
			session[:city_id] = nil
			session[:location] = nil
			session[:location_id] = nil
		end
		
		if params[:location_id].present?
			location_name = Location.where(:id => params[:location_id]).first.name rescue nil
			session[:location] = location_name
			session[:search_string] += " near #{location_name}"
		else
			session[:location] = nil
			session[:location_id] = nil
		end
	end
	

end
