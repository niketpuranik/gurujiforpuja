class AutocompleteController < ApplicationController
	def get_pujas
		list = []
		services = PUJA_CATEGORIES[4]
		if params[:term].present?
			like = "%#{params[:term]}%"
			
			#list = Puja.where("name like ?",like).limit(10).map do |x| 
			list =	Puja.where("name like ?", like).where("category not like ?", services).limit(10).map do |x| 
					name  = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		else
			list = Puja.order("name asc").where("category not like ?", services).limit(10).map do |x| 
				name = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		end
		list.uniq!
		render :json  => list
	end
	
	def get_locations
		list = []
		scope = Location
		scope = scope.where(:city_id => params[:city_id]) if params[:city_id].present?
		if params[:term].present?
			like = "#{params[:term]}%"
			list = scope.where("name like ?",like).limit(10).map do |x| 
				name  = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		else
			list = scope.order("name asc").limit(10).map do |x| 
				name = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		end
		list.uniq!
		render :json  => list
	end
	
	def get_cities
		list = []
		if params[:term].present?
			like = "#{params[:term]}%"
			list = City.where("name like ?",like).limit(10).map do |x| 
				name  = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		else
			list = City.order("name asc").limit(10).map do |x| 
				name = x.name rescue ''
				{:label => name, :name => name, :id => x.id}
			end
		end
		list.uniq!
		render :json  => list
	end

end
