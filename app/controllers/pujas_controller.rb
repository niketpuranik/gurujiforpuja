class PujasController < ApplicationController
	load_and_authorize_resource :except => [:index, :show]
	layout "dashboard", :except => [ :show] 
  # GET /pujas
  # GET /pujas.json
  def index
    @pujas = Puja.metasearch(params[:search]).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pujas }
    end
  end

  # GET /pujas/1
  # GET /pujas/1.json
  def show
    @puja = Puja.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @puja }
    end
  end

  # GET /pujas/new
  # GET /pujas/new.json
  def new
    @puja = Puja.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @puja }
    end
  end

  # GET /pujas/1/edit
  def edit
    @puja = Puja.find(params[:id])
  end

  # POST /pujas
  # POST /pujas.json
  def create
    @puja = Puja.new(params[:puja])

    respond_to do |format|
      if @puja.save
        format.html { redirect_to @puja, notice: 'Puja was successfully created.' }
        format.json { render json: @puja, status: :created, location: @puja }
      else
        format.html { render action: "new" }
        format.json { render json: @puja.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pujas/1
  # PUT /pujas/1.json
  def update
    @puja = Puja.find(params[:id])

    respond_to do |format|
      if @puja.update_attributes(params[:puja])
        format.html { redirect_to @puja, notice: 'Puja was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @puja.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pujas/1
  # DELETE /pujas/1.json
  def destroy
    @puja = Puja.find(params[:id])
    @puja.destroy

    respond_to do |format|
      format.html { redirect_to pujas_url }
      format.json { head :no_content }
    end
  end
end
