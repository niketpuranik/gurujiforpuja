class ApplicationController < ActionController::Base
  protect_from_forgery
  
  rescue_from CanCan::AccessDenied, :with => :access_denied
  
  helper_method :get_meta_info

	helper_method :current_session_id
	helper_method :current_user_id
  
  private
  
  def current_session_id
		request.session_options[:id]
	end
	
	def current_user_id
		current_user ? current_user.id : nil
	end

  def access_denied
  	if current_user
			flash[:notice] = "You are not authorized to access this page!"
			redirect_to "/"
  	else
  		flash[:notice] = "You must login to access this page!"
			redirect_to new_user_session_path
  	end
  end
  
  def get_meta_info map=nil
  	if(map.nil?)
  		map = {}
  	end
  	if(map[:title].nil? or map[:title].blank?)
			title = DEFAULT_TITLE
		else
			title = map[:title]
		end
	
		if(map[:description].nil? or map[:description].blank?)
			description = DEFAULT_DESCRIPTION
		else
			description = map[:description]
		end
	
		if(map[:keywords].nil? or map[:keywords].blank?)
			keywords = DEFAULT_KEYWORDS.join(",")
		else
			keywords = map[:keywords]
		end	
	
  	return {:title=>title,:keywords=>keywords,:description => description}
  end
end
