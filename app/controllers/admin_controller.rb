class AdminController < ApplicationController
	before_filter :check_admin
	
	layout "dashboard"
	
	def users
		@users = User.metasearch(params[:search]).page(params[:page]).per(10)
		@search = User.metasearch(params[:search])
	end

	def gurujis
		@gurujis = Guruji.metasearch(params[:search]).page(params[:page]).per(10)
		@search = Guruji.metasearch(params[:search])
	end
	
	def approve_user
		@user = User.find(params[:user_id])
		if params[:enable] == "true"
			@user.approved = true
		else
			@user.approved = false
		end
		@success = @user.save
		respond_to do |format|
			format.js
		end
	end

	
	
	private 
	def check_admin
		authenticate_user!
		if current_user.is_a? SuperAdmin
		else
			flash[:notice] = "You are not authorized to access this page!"
			redirect_to(root_url)
		end
	end
end
