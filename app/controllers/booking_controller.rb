class BookingController < ApplicationController
	def new_booking
		if params[:event_id].present?
			@booking  = Booking.new(:prefered_time => DailyEvent.find(params[:event_id]).from_time) rescue Booking.new()
		elsif params[:prefered_time].present?
			pt = DateTime.parse(params[:prefered_time]) rescue Time.now
			@booking  = Booking.new(:prefered_time => pt) rescue Booking.new()
		else
			@booking  = Booking.new(:prefered_time => Time.now) rescue Booking.new()
		end
		
		if current_user
			@booking.name = current_user.name
			@booking.email = current_user.email
		end
		
		if session[:puja_id].present?
			@booking.puja_id = session[:puja_id]
		end
		
		if params[:puja_id].present?
			@booking.puja_id = params[:puja_id]
		end
		
		if session[:city_id].present?
			@booking.city_id = session[:city_id]
		end
		
		if params[:city_id].present?
			@booking.city_id = params[:city_id]
		end
		
		if session[:location_id].present?
			@booking.location_id = session[:location_id]
		end
		
		@events = DailyEvent.where("from_time >= ? and from_time <= ? and puja_ids like ?", @booking.prefered_time, (@booking.prefered_time + 1.month),"%#{params[:puja_id]}%")
		respond_to do |format|
			format.html { render :layout => false }
		end
	end

	def new_services_booking
		if params[:event_id].present?
			@booking  = Booking.new(:prefered_time => DailyEvent.find(params[:event_id]).from_time) rescue Booking.new()
		elsif params[:prefered_time].present?
			pt = DateTime.parse(params[:prefered_time]) rescue Time.now
			@booking  = Booking.new(:prefered_time => pt) rescue Booking.new()
		else
			@booking  = Booking.new(:prefered_time => Time.now) rescue Booking.new()
		end
		
		if current_user
			@booking.name = current_user.name
			@booking.email = current_user.email
		end
		
		if session[:puja_id].present?
			@booking.puja_id = session[:puja_id]
		end
		
		if params[:puja_id].present?
			@booking.puja_id = params[:puja_id]
		end
		
		if session[:city_id].present?
			@booking.city_id = session[:city_id]
		end
		
		if params[:city_id].present?
			@booking.city_id = params[:city_id]
		end
		
		if session[:location_id].present?
			@booking.location_id = session[:location_id]
		end
		
		@events = DailyEvent.where("from_time >= ? and from_time <= ? and puja_ids like ?", @booking.prefered_time, (@booking.prefered_time + 1.month),"%#{params[:puja_id]}%")
		respond_to do |format|
			format.html { render :layout => false }
		end
	end
	
	def edit_booking
		@booking  = Booking.find(params[:booking_id])
		respond_to do |format|
			format.html { render :layout => false }
		end
	end
	
	def create_booking
		@booking  = Booking.new(params[:booking])
		@success = @booking.save
		respond_to do |format|
			format.js
		end
	end
	
	def update_booking
		@booking  = Booking.find(params[:booking_id])
		respond_to do |format|
			format.js
		end
	end
	
	def cancel_booking
		@booking  = Booking.find(params[:booking_id])
		respond_to do |format|
			format.html { render :layout => false }
		end
	end
	
	def show_booking
		@booking  = Booking.find(params[:booking_id])
		respond_to do |format|
			format.html { render :layout => false }
		end
	end
end
