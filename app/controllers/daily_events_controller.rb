class DailyEventsController < ApplicationController
	load_and_authorize_resource :except => [:index, :show, :get_daily_events]
	
	layout "dashboard", :except => [:show, :get_daily_events]
  # GET /daily_events
  # GET /daily_events.json
  def index
    @daily_events = DailyEvent.page(params[:page]).per(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @daily_events }
    end
  end
  
  def get_daily_events
    @daily_events = DailyEvent.metasearch(params[:search]).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => { :success => true, :events => @daily_events.to_json(:methods => [:url])} }
    end
  end

  # GET /daily_events/1
  # GET /daily_events/1.json
  def show
    @daily_event = DailyEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @daily_event }
    end
  end

  # GET /daily_events/new
  # GET /daily_events/new.json
  def new
    @daily_event = DailyEvent.new
		@daily_event.from_time = Date.today
		@daily_event.to_time = Date.today + 23.hours + 59.minutes	
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @daily_event }
    end
  end

  # GET /daily_events/1/edit
  def edit
    @daily_event = DailyEvent.find(params[:id])
  end

  # POST /daily_events
  # POST /daily_events.json
  def create
    @daily_event = DailyEvent.new(params[:daily_event])

    respond_to do |format|
      if @daily_event.save
        format.html { redirect_to @daily_event, :notice => 'Daily event was successfully created.' }
        format.json { render :json => @daily_event, :status => :created, :location => @daily_event }
      else
        format.html { render :action => "new" }
        format.json { render :json => @daily_event.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /daily_events/1
  # PUT /daily_events/1.json
  def update
    @daily_event = DailyEvent.find(params[:id])

    respond_to do |format|
      if @daily_event.update_attributes(params[:daily_event])
        format.html { redirect_to @daily_event, :notice => 'Daily event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @daily_event.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /daily_events/1
  # DELETE /daily_events/1.json
  def destroy
    @daily_event = DailyEvent.find(params[:id])
    @daily_event.destroy

    respond_to do |format|
      format.html { redirect_to daily_events_url }
      format.json { head :no_content }
    end
  end
end
