module LocationsHelper
	def get_locations
		h = {}
		Location.all.each{|l| h[l.name] = l.id}
		return h
	end
end
