module PujasHelper
	def get_pujas
		h = {}
		Puja.where("category not like ?", PUJA_CATEGORIES[4]).each{|p| h[p.name] = p.id}
		return h
	end
	def get_services
		h = {}
		Puja.where(:category => "Services").each{|p| h[p.name] = p.id}
		return h
	end
end
