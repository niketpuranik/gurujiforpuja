module CountriesHelper
	def get_countries
		h = {}
		Country.all.each{|c| h[c.name] = c.id}
		return h
	end
end
