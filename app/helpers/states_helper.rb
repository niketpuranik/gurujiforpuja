module StatesHelper
	def get_states
		h = {}
		State.all.each{|c| h[c.name] = c.id}
		return h
	end

end
