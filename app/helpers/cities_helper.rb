module CitiesHelper
	def get_cities
		h = {}
		City.all.each{|c| h[c.name] = c.id}
		return h
	end
end
