module PlansHelper
	def get_plans
		h = {}
		Plan.active.each{|p| h[p.name] = p.id }
		return h
	end
end
