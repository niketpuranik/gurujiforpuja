module ApplicationHelper
	def get_domain
		if Rails.env == "production"
			return "http://gurujiforpuja.com"
		else
			return "http://#{LOCAL_IP}"
		end
	end
	
	def newpass( len )
    chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
    newpass = ""
    1.upto(len) { |i| newpass << chars[rand(chars.size-1)] }
    return newpass
	end

	def get_guruji
		return "test helper"
	end
end
