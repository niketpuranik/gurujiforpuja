
<div><h2>Numerology</h2></div>
<hr/>
<p><strong>What is Numerology</strong></p>
					<p>Numerology is the metaphysical science-or philosophy-of numbers asserting that every object and living being within the universe can be explained, revealed and/or reflected through numbers. It is linked to the ancient mathematicians' beliefs that the foundation of the entire universe is built on and of numbers. Not unlike the core of computer intelligence, where the entire language is a combination of the numbers zero and one, numerology analyzes combinations of the numbers 1 through 9 to determine a language or science of its own.</p>
					<p>Basically, numerology assigns a single-digit numerical value to every letter, month, day, year, etc. and then uses these numbers to predict and analyze. For instance, all the letters in your name can be translated into numbers, then added and reduced to a single-digit number that reveals your true self.</p>

					<p>Most numerologists assert that each of us has been given a unique name intended just for us that reflects our own personal spiritual vibration or essence. And in the way that your name reflects your personal vibration, your birth date reveals your destiny. Still, there are other numerologists who believe that as our names and the years change so, too, do we. Nothing is absolutely sacred or carved in stone.</p>

					<p>Whatever their personal belief, most numerologists believe very much in anything spiritual and/or divine. They promote the ideas of karma, past lives, destiny, souls, etc. This, unfortunately, can scare away many potential followers. I, myself, tend to be skeptical of most things in life and have not, as of yet, determined my complete spiritual belief system. But I don't think you are required to adopt all of numerology's assumptions in order to use and enjoy numerology. An open mind is all that is needed. You can decide later-and on your own terms-what your spiritual beliefs are.</p>

<p>As a quick example, I will demonstrate how to translate a name into a number. Using the Pythagorean chart I can change each letter of my name into a number.</p>

<p><b>We would then add up all of those numbers:<br/>
= 1 + 9 + 5 +4 + 9 + 9 + 5 + 2 + 8<br/>
= 52<br/>
= 5 + 2<br/>
= 7<br/>
</b></p>
<p>The vibration or number of my Magickal name, then, is 7. CONGRATULATIONS!! You have just mastered the fundamental aspect of numerology.</p>

<p><b>The Meaning of Numbers</b></p>
<p>In numerology, each of the numbers one through nine has an inherent meaning that is found within its vibration. The meaning of each of the numbers is the basis of numerological analysis. Before we learn how to calculate our personal numbers, let's take a closer look at what each number symbolizes or represents. In this section I will briefly describe the meaning of each number. The Complete Idiot's Guide To Numerology, however, has a much more in-depth look at all of the numbers.</p>

<p><b>The Number One</b></p>
<p>The first of all numbers, The Number One is also the Leader of all numbers. People with the number one in their charts are creative, energetic, independent, and intuitive. These are the people that exude self-confidence from their every pore and silently beckon us to follow them. But even though number ones tend to be very generous, they can also become self-obsessed and insolent, deluding themselves into believing they are almighty and all-knowing.</p>

<p><b>The Number Two</b></p>
<p>Almost the complete opposite of the number one, The Number Two represents synergy and diplomacy. Not interested in becoming great leaders, people with the number two in their charts are content to work behind the scenes supporting and encouraging others. Gentle, sweet, supportive and loving, number twos have a tendency to allow others to walk all over them and influence their already fragile self-confidence and self-esteem. Depression and low self-confidence may become a problem for the number two.</p>

<p><b>The Number Three</b></p>
<p>The Number Three is exuberance incarnate. Happy-go-lucky and full of life, people with a number three in their charts are the consummate "life of the party!" Lucky, creative, and very extroverted, number threes love the finer things in life, spending money as quickly as they receive it. A flirt with his or her head in the clouds (can you say "delusions of grandeur?"), the three person has the negative tendency to become overly critical and sharp-tongued. Their moods are also known to fluctuate.</p>

<p><b>The Number Four</b></p>
<p>The Number Four represents pragmatism, security and stability. A hard worker with a practical mind geared toward the future, a person with the number four in their chart is the quintessential "head of the family" who looks after those he or she cares about. The number four's strengths are its steadfastness and tenacity; its weaknesses, rigidity and lifelessness. Along with that streak of practicality comes the tendency to be a bit of a bore.</p>

<p><b>The Number Five</b></p>
The Number Five symbolizes change, rebellion, and freedom. People with the number five in their charts value freedom above all else, oftentimes rebelling against the status quo, going where no one has dared go before, and refusing to allow others to dictate any control over them. Number fives often have trouble with commitment and can be talented at many things, but specialized in none. Because of this lack of commitment, people with the number five in their charts can begin to feel overwhelmed, having started many projects but finishing none.</p>

<p><b>The Number Six</b></p>
<p>The Number Six represents the home, family, love, beauty and romance. People with the number six in their charts are very loving, kind, and gentle. They are affectionate and tender and are natural born parents. And yet, since most of their decisions come from their heart it may be impossible to reach these soul creatures with reason. Add to this their unrelenting and fixed natures, and they can become difficult to live with, possibly even leaving the nest if they feel the love is no longer deep enough to sustain them.</p>

<p><b>The Number Seven</b></p>
<p>The Number Seven is often seen as the most eccentric of all the numbers. Mystical, deep, analytical, wise and intuitive, people with the number seven in their chart are the quintessential loners who seemingly abandon society in exchange for their own personal quests of knowledge and wisdom. But the number seven has many gifts and talents to share with the world and often makes a wonderful teacher. The number seven can become a bit out of touch with the needs of others if they withdraw too much; a balance should be sought.</p>

<p><b>The Number Eight</b></p>
<p>The Number Eight is one of the most powerful numbers in numerology. People with the number eight in their charts are born for achievement, success and leadership. They thrive on rising through a hierarchal organization and manipulating those working alongside of them to become part of their own vessel. They love money and grandeur and may stop at nothing to achieve their desired success.</p>

<p><b>The Number Nine</b></p>
<p>As the number one is the beginning of all things, the Number Nine is the end. It represents humanitarianism, compassion, tolerance and both spiritual and material success. People with the number nine in their charts are idealistic mystics that long to leave the world a better place. They are blessed with more charm and compassion than all of the numbers added together. Unfortunately, they can become too idealistic, forcing their beliefs onto others and/or becoming prejudiced against those who do not follow the path they themselves have chosen.</p>

<p><b>The Master Numbers</b></p>
<p>In addition to the numbers one through nine, there are numbers that numerologists refer to as master numbers. These numbers are 11, 22, 33, 44, 55, 66, 77, 88, and 99. For this course we will look only at the master numbers eleven and twenty-two. The higher master numbers are quite rare in the Western world.</p>

<p>It is said that the master numbers operate at a much higher vibration than the numbers one through nine, and that they also include the vibration of their single-digit reduction. For instance, the master number eleven would be written as 11/2, and consequently carries both the vibrations for eleven and two (even a double dose of ones).</p>

<p>People with a master number in their charts are called upon to bring many great gifts to the world. As noted in their book, The Complete Idiot's Guide To Numerology, Kay Lagerquist and Lisa Lenard tell us that many numerologists believe that if you have a master number in your chart you have probably made a pact with the higher power to return to earth for the betterment of humanity.</p>

