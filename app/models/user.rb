class User < ActiveRecord::Base
	
	extend FriendlyId
  friendly_id :name, use: :slugged
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, 
         :validatable, :token_authenticatable, 
         :confirmable, :lockable, :timeoutable, :omniauthable

	attr_accessor :info, :credentials, :extra

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,:first_name, :last_name
  attr_accessible :phone, :type, :approved, :experience, :offered_pujas, :offered_services, :cities, :locations, :accept_terms
  attr_accessible :info, :credentials, :extra, :priority
  
  attr_accessor :plan_id, :accept_terms
  
	attr_accessible :plan_id, :description
  
  validates :first_name, :last_name, :presence => true
  
  has_one :photo, :as => :attachable, :class_name => "Image", :dependent => :destroy, :conditions => { :group_id => "photo" }
  
  def name
  	return [first_name,last_name].compact.join(" ")
  end
  
  # Overrides the devise method find_for_authentication
	# Allow users to Sign In using their username or email address
	def self.find_for_authentication(conditions)
		where(conditions).where(:approved => true).first
	end
	
	has_many :subscriptions, :foreign_key => "user_id"
	
	has_many :active_subscription, :class_name => "Subscription", :foreign_key => "user_id", :conditions => { :status => ACTIVE }
	
	has_many :user_tokens, :dependent => :destroy
	
	def plan
		return active_subscription.first.plan rescue nil
	end
	
	
	
	def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
  	email = auth.info.email
		user = User.readonly(false).joins(:user_tokens).where("user_tokens.provider" => auth.provider, "user_tokens.uid" => auth.uid).first
		if not user
			user = User.readonly(false).where(:email => email).first
		end
		if signed_in_resource and not user
			user = signed_in_resource
		end
		if not user
			fname = auth['info']['name'].split(" ")[0] rescue '-'
			lname = auth['info']['name'].split(" ")[1] rescue '-'
		  user = User.new(
		                     :first_name => fname,
		  									 :last_name => lname,
	                       :email => email,
	                       :password => Devise.friendly_token[0,20]
	                       )
	    user.skip_confirmation!                  
	    user.save
		else
			user.apply_omniauth(auth)
		end
		user
	end
	
	#config omniauth twitter
	def self.find_for_twitter_oauth(auth, signed_in_resource = nil)
	  data = auth.extra.raw_info
	  email = auth.info.email
		
	  user = User.readonly(false).joins(:user_tokens).where("user_tokens.provider" => auth.provider, "user_tokens.uid" => auth.uid).first
		if not user
			user = User.readonly(false).where(:email => email).first
		end
		if signed_in_resource and not user
			user = signed_in_resource
		end
	  if not user
			fname = auth['info']['name'].split(" ")[0] rescue '-'
			lname = auth['info']['name'].split(" ")[1] rescue '-'
		  user = User.new(
		                     :first_name => fname,
		  									 :last_name => lname,
	                       :email => email,
	                       :password => Devise.friendly_token[0,20]
	                       )
	    user.skip_confirmation!                  
	    user.save
		else
			user.apply_omniauth(auth)
		end
		user
	end
	
	#config omniauth twitter
	def self.find_for_google_oauth(auth, signed_in_resource = nil)
	  data = auth.info
	  email = data["email"]
	  name = data["name"]
	  
	  user = User.readonly(false).joins(:user_tokens).where("user_tokens.provider" => auth.provider, "user_tokens.uid" => auth.uid).first
		if not user
			user = User.readonly(false).where(:email => email).first
		end
		if signed_in_resource and not user
			user = signed_in_resource
		end
	  if not user
			fname = name.split(" ")[0] rescue '-'
			lname = name.split(" ")[1] rescue '-'
		  user = User.new(
		                     :first_name => fname,
		  									 :last_name => lname,
	                       :email => email,
	                       :password => Devise.friendly_token[0,20]
	                       )
	    user.skip_confirmation!                  
	    user.save
		else
			user.apply_omniauth(auth)
		end
		user
	end
  
	def apply_omniauth(omniauth)
    case omniauth['provider']
			when 'facebook'
				self.apply_facebook(omniauth)
			when 'twitter'
				self.apply_twitter(omniauth)	
			when 'google_oauth2'
				self.apply_google(omniauth)	
		end
		token = user_tokens.where(:provider => omniauth['provider']).first
		if not token
			user_tokens.build(:provider => omniauth['provider'], :uid => omniauth['uid'], :token =>(omniauth['credentials']['token'] rescue nil))
		end
		self.save
	end
	
	def facebook
		@fb_user ||= FbGraph::User.me(self.user_tokens.find_by_provider('facebook').token)  rescue nil
	end
	
	def apply_facebook(omniauth)
		if (extra = omniauth['extra']['user_hash'] rescue false)
		  self.email = (extra['email'] rescue '')
		end
	end
	
	def apply_twitter(omniauth)
	end
	
	def apply_google(omniauth)
	end
	
	def self.new_with_session(params, session)
    super.tap do |user|
      if data = session[:omniauth]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
	
	
	before_create :before_user_create
	
	after_create :after_guruji_create

	before_create :add_subscription 
	
	private
	def before_user_create
		if self.type == "Guruji"
		else
			self.approved = true
		end
	end
	
	def add_subscription
		if self.type == "Guruji"
			Rails.logger.info "***************** Adding subscription to user #{self.plan_id} ************************"
			s = Subscription.new(:plan_id => self.plan_id, :status => ACTIVE)
			self.subscriptions.push s
		end
		return true
	end
	
	def after_guruji_create
		if self.type == "Guruji"
			begin
				Notifier.notify_guruji_signup(self).deliver!
			rescue Exception => e
				Rails.logger.info "ERROR IN AFTER GURUJI CREATE :: #{e.message}"
			end
		end
	end
end
