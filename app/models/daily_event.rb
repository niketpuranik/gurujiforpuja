class DailyEvent < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :description, :entity_id, :entity_type, :from_time, :name, :tags, :to_time, :puja_ids
  
  validates :name, :description, :from_time, :to_time, :presence => true
  
  has_one :photo, :as => :attachable, :class_name => "Image", :dependent => :destroy, :conditions => { :group_id => "photo" }
  
  def pujas
  	Puja.where(:id => (self.puja_ids.split("|") rescue []))
  end

  def single_url(label=:mini)
  	return 	pujas.collect{|x| x.photo.url(:mini) rescue '/assets/ganpati.png'}.first
  	#return (photo.url(label) rescue "/assets/ganpati.png")
  end
  
  def puja_url(puja_id)
  	Puja.find(puja_id).photo.url(:mini) rescue '/assets/ganpati.png'
  end
  
  def url(label=:mini)
  	return 	pujas.collect{|x| x.photo.url(:mini) rescue '/assets/ganpati.png'}.join("|")
  	#return (photo.url(label) rescue "/assets/ganpati.png")
  end
  
  
  def self.generate_events(from_day,days)
  	r = (from_day)..(from_day + days.days)
  	arr = [*r]
  	arr.each do |day|
  		e = self.new
  		e.from_time = day
  		e.to_time = day + 23.hours + 59.minutes
  		e.name = day.to_s
  		e.description = day.to_s
  		e.save!
  	end
  end
end
