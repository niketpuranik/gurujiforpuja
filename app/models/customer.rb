class Customer < User
	has_many :bookings, :foreign_key => :user_id
end
