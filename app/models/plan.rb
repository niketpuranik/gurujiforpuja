class Plan < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :description, :duration, :name, :price, :active
  
  validates :name, :price, :active, :duration, :presence => true
  
  scope :active, where(:active => true)
  
  attr_accessor :options_hash

  @@option_variables = {
  									 "pujas_limit" => "integer",
  									 "photos_limit" => "integer",
  									 "videos_limit" => "integer",
  									 "cities_limit" => "integer",
  									 "locations_limit" => "integer",
  									 "services_limit" => "integer",
  								 }

  @@option_variables.each do |method,type|
  	attr_accessible method.to_sym
    send :define_method, "#{method.to_sym}" do
	    self.options_hash = {}
	    if self.options.present?
	      self.options_hash = ActiveSupport::JSON.decode(self.options)
      end 
      case type
    		when "boolean"
	      	return (((self.options_hash[method].to_i == 0) ? false : true) rescue false )
      	when "integer"
	      	return self.options_hash[method].to_i
	      else
	      	return self.options_hash[method]
      end
    end

    self.send(:define_method, "#{method.to_s}=") do |val|
    	self.options_hash = {}
      self.options_hash = ActiveSupport::JSON.decode(self.options) if self.options.present?
      self.options_hash[method] = val
      self.options = self.options_hash.to_json
      return val
    end
  end

  def reset_options_hash
		self.save
		return true
	end
  
  def self.option_variables
    @@option_variables
  end
end

