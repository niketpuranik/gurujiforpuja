class Contact < ActiveRecord::Base
  attr_accessible :comment, :email, :fname, :lname, :phone ,:contact_type
  
  validates :fname, :email, :phone, :presence => true
  validates :phone,:numericality => true, :allow_blank => true
  validates_format_of :email, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
end
