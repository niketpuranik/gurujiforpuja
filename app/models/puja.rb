class Puja < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :category, :description, :name, :process, :samugri
  
  
  validates :name, :samugri, :category, :description, :presence => true
  
  has_one :photo, :as => :attachable, :class_name => "Image", :conditions => { :group_id => "photo" }, :dependent => :destroy
  
  has_many :bookings
end
