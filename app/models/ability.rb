class Ability
	include CanCan::Ability
  def initialize(user)
		user ||= User.new # guest user
		can [:show], :all
    if user.is_a? SuperAdmin
    	can :manage, :all
    elsif user.is_a? Guruji
        can :manage, :all
    end
    can :read, :all
    can :manage, Asset do |asset|
    	asset.user_id == user.id
    end
    can :manage, Video do |asset|
    	asset.user_id == user.id
    end		
 	end
end
