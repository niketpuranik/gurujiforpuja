class Subscription < ActiveRecord::Base
  attr_accessible :plan_id, :repeat_plan, :start_date, :status, :user_id
  
  validates :plan_id, :status, :user_id, :presence => true 
  
  validates :status, :inclusion => SUBSCRIPTION_STATUSES
  
  belongs_to :plan
  belongs_to :guruji, :foreign_key => "user_id"
end
