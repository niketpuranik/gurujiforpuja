class Guruji < User
	has_many :images, :as => :attachable, :class_name => "Image", :dependent => :destroy, :conditions => { :group_id => "images" }
	
	has_many :videos, :foreign_key => :user_id, :dependent => :destroy
	
	has_many :bookings, :foreign_key => :guruji_id
	
	has_many :daily_trends
	
	attr_accessible :profile_views, :search_views, :visited
		
	def todays_trend
		dt = self.daily_trends.where("created_at >= ? and created_at <= ?",Date.today, Date.today+1.day).first
		if not dt
			dt = DailyTrend.create!(:guruji_id => self.id, :pvc => 0, :svc => 0)
		end
		return dt
	end
	
	def increment_search_views!
		self.update_attributes!(:visited => true, :search_views => (self.search_views.to_i + 1))
		dt = todays_trend 
		dt.update_attributes!(:svc => (dt.svc.to_i + 1))
	end
	#handle_asynchronously "increment_search_views!"
	
	def increment_profile_views!
		self.update_attributes!(:profile_views => (self.profile_views.to_i + 1))
		dt = todays_trend 
		dt.update_attributes!(:pvc => (dt.pvc.to_i + 1))
	end
	#handle_asynchronously "increment_profile_views!"
end
