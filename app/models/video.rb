class Video < ActiveRecord::Base
  attr_accessible :description, :name, :tags, :url, :user_id

	belongs_to :guruji, :foreign_key => :user_id
	
	validates :name, :url, :presence => true
  
  before_create :check_count
	
	private
	
	def check_count
		if self.guruji.plan and self.guruji.plan.videos_limit > self.guruji.videos.count
			return true
		else
			self.errors.add :videos, "Your maximum video upload limit is reached."
			return false
		end 
	end
end
