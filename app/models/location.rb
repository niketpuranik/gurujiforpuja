class Location < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :city_id, :name
  validates :city_id, :name, :presence => true
  belongs_to :city
  
  has_many :bookings
end
