include ApplicationHelper
class Booking < ActiveRecord::Base
  attr_accessible :address, :city_id, :location_id, :prefered_time, :puja_id, :user_id, :guruji_id, :note
  attr_accessible :name, :email, :phone, :password
  
  
  attr_accessor :name, :email, :phone, :password
  
  belongs_to :customer, :foreign_key => :user_id
  belongs_to :guruji, :foreign_key => :guruji_id
  
  belongs_to :location
  belongs_to :city
  belongs_to :puja
  
  validates :name, :email, :phone, :city_id, :location_id, :prefered_time, :address, :puja_id, :presence => true
  


	def address_string
		[address,(location.name rescue '-'),(city.name rescue '-')].join(", <br/>")
  end


  before_create :create_user
  after_create :send_mails
  private
  
  def send_mails
  	Notifier.booking_confirmation_to_admin(self).deliver
  	Notifier.booking_confirmation_to_customer(self).deliver
  end
  
  def create_user
  	fn = self.name.split(" ")[0] rescue '-'
  	ln = self.name.split(" ")[1] rescue '-'
  	fn = "-" if not fn.present?
  	ln = "-" if not ln.present?
  	
		user = User.where(:email => self.email).first
  	if not user
			user = Customer.new(:first_name => fn, :last_name => ln, :email => self.email, :phone => self.phone)  		
			user.approved = true
			
			pass = newpass(6)
			
			user.password = pass
			
			user.skip_confirmation!
			
			self.password = pass
			
			if user.save
				self.user_id = user.id
				return true
			else
				user.errors.each do |msg,error|
					self.errors.add msg, error
				end
				return false
			end
		else
			if user.is_a? Customer
				self.user_id = user.id
				return true
			else
				self.errors.add :user, "You are not allowed to book for Puja."
				return false
			end
		end
  end
end
