require 'test_helper'

class DailyEventsControllerTest < ActionController::TestCase
  setup do
    @daily_event = daily_events(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:daily_events)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create daily_event" do
    assert_difference('DailyEvent.count') do
      post :create, :daily_event => { :description => @daily_event.description, :entity_id => @daily_event.entity_id, :entity_type => @daily_event.entity_type, :from_time => @daily_event.from_time, :name => @daily_event.name, :tags => @daily_event.tags, :to_time => @daily_event.to_time }
    end

    assert_redirected_to daily_event_path(assigns(:daily_event))
  end

  test "should show daily_event" do
    get :show, :id => @daily_event
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @daily_event
    assert_response :success
  end

  test "should update daily_event" do
    put :update, :id => @daily_event, :daily_event => { :description => @daily_event.description, :entity_id => @daily_event.entity_id, :entity_type => @daily_event.entity_type, :from_time => @daily_event.from_time, :name => @daily_event.name, :tags => @daily_event.tags, :to_time => @daily_event.to_time }
    assert_redirected_to daily_event_path(assigns(:daily_event))
  end

  test "should destroy daily_event" do
    assert_difference('DailyEvent.count', -1) do
      delete :destroy, :id => @daily_event
    end

    assert_redirected_to daily_events_path
  end
end
