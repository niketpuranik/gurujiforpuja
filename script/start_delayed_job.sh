#!/bin/bash
source ~/.rvm/environments/default

# Go to the app
APP_HOME=/home/prashant/linode/projects/gurujipooja
cd $APP_HOME

# start the delayed jobs
RAILS_ENV=production ruby script/delayed_job start  
