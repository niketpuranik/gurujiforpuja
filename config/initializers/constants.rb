FACEBOOK_URL = "https://www.facebook.com/pages/Gurujiforpuja/446023175460151"
TWITTER_URL = "http://twitter.com/GurujiforPuja"

USER_TYPES = ["Guruji","User"]

NOTIFIER_RECEIVERS = ["admin@truespider.com","director@truespider.com"]

PUJA_CATEGORIES = ["General","House Warming","Festival","Special Puja","Services"]

ACTIVE = "active"
INACTIVE = "inactive"
CANCELLED = "cancelled" 
SUBSCRIPTION_STATUSES = [ACTIVE,INACTIVE,CANCELLED]

DEFAULT_TITLE = "Get a Guruji / Swami / Pandit for any Puja easily and quickly. Guruji for puja, Pandit for puja, Swami for puja, Guruji for vidhi, Pandit for vidhi, Swami for vidhi, Grihapravesh, Satya Narayan, Vastu Shanti, Marriage, Lagna Samarambha, Munja, Shradh"

DEFAULT_DESCRIPTION = "Guruji for puja is a portal that provides an innovative and faster way to get a guruji for any type of puja. Here you can get information about all the pujas/vidhis that people do. Vastu Shanti, Griha-Pravesh, Marriage Rituals, Satya Narayan, Munj, Shradh and many more types of pujas and vidhis are perormed in indian culture for different occassions. Finding a Swami, Pandit or a Guruji for performing these rituals is often difficult for new people in metro cities. Guruji For Puja provides a unique and easy way for all to get an experienced, and well known gurujis in your town."

DEFAULT_KEYWORDS = ["Guruji for puja", "Pandit for puja", "Swami for puja", "Guruji for vidhi", "Pandit for vidhi", "Swami for vidhi", "Grihapravesh", "Satya Narayan", "Vastu Shanti", "Marriage", "Lagna Samarambha", "Munja", "Shradh"] 

SERVICES = {
	"Palmist" => "palmist",
	"Numerology" => "numerology",
	"Astrology" => "astrology",
	"Horoscope" => "horoscope",
	"Vastu Shastra" => "vastu_shastra",
	"Gem Finder" => "gem_finder",
	"Tarot Card" => "tarot_card",
	"Reiki"	=> "reiki",
	"Feng Shui" => "feng_shui",
	"Agni Upasana" => "agni_upasana",
	"Crystal Healing" => "crystal_healing"
}

PLANS_SEARCH_COUNT = {
	5000 => 3,
	2000 => 3,
	1000 => 2,
	800 => 1,
	600 => 1,
}

CONTACT_TYPE = ["Feedback","Question","Problem"]
