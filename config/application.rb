require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

local_ip = "localhost:3001"
begin
        orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse DNS resolution temporarily
        UDPSocket.open do |s|
                s.connect '64.233.187.99', 1
                local_ip = s.addr.last + ":3001"
                puts "Starting on #{local_ip}"
        end
rescue Exception => e
        puts "ERROR :: Failed to get local_ip :: #{e.message}"
ensure
        Socket.do_not_reverse_lookup = orig
end

#local_ip = "gurujiforpuja.com"
LOCAL_IP = local_ip

module Gurujipooja
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    # config.autoload_paths += %W(#{config.root}/extras)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    
    if Rails.env == 'development'
			config.action_mailer.default_url_options = { :host => "#{LOCAL_IP}", :only_path => false, :protocol => "http" }
			config.action_controller.asset_host = "http://#{LOCAL_IP}"
			config.action_mailer.asset_host = "http://#{LOCAL_IP}"
		elsif Rails.env == 'test'
			config.action_mailer.default_url_options = { :host => "#{LOCAL_IP}", :only_path => false, :protocol => "http" }
			config.action_controller.asset_host = "http://#{LOCAL_IP}"
			config.action_mailer.asset_host = "http://#{LOCAL_IP}"
		elsif Rails.env == 'staging' || Rails.env == 'production'
			config.action_mailer.default_url_options = { :host => "gurujiforpuja.com", :only_path => false, :protocol => "http" }
			config.action_controller.asset_host = "http://gurujiforpuja.com"
			config.action_mailer.asset_host = "http://gurujiforpuja.com"

			config.middleware.use ExceptionNotifier,
			  :email_prefix => "[Guruji For Puja ERROR] ",
			  :sender_address => '"Admin" <admin@truespider.com>',
			  :exception_recipients => ['prashant.chaudhari89@gmail.com','puja@truespider.com','niket@truespider.com']
		else
			config.action_mailer.default_url_options = { :host => 'gurujiforpuja.com', :only_path => false, :protocol => "http" }	
			config.action_controller.asset_host = "http://gurujiforpuja.com"
			config.action_mailer.asset_host = "http://gurujiforpuja.com"
		end

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable escaping HTML in JSON.
    config.active_support.escape_html_entities_in_json = true

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    config.active_record.whitelist_attributes = true

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'
    
    config.time_zone = 'Mumbai'
    config.active_record.default_timezone = :local
  end
end
Time::DATE_FORMATS[:default] = "%Y/%m/%d %H:%M %p"
Date::DATE_FORMATS[:default] = "%Y/%m/%d"
Time::DATE_FORMATS[:for_edit] = "%Y/%m/%d %H:%M %p"
Date::DATE_FORMATS[:for_edit] = "%Y/%m/%d"

Time::DATE_FORMATS[:show] = "%d/%m/%Y %H:%M %p"
Date::DATE_FORMATS[:show] = "%d/%m/%Y"
Time::DATE_FORMATS[:date_show] = "%d/%m/%Y"

Date::DATE_FORMATS[:time] = "%I:%M %p"
Time::DATE_FORMATS[:time] = "%I:%M %p"

Date::DATE_FORMATS[:calendar] = "%m/%d/%Y"
Time::DATE_FORMATS[:calendar] = "%m/%d/%Y %H:%M"


Time::DATE_FORMATS[:date] = "%Y/%m/%d"

require "smtp_tls"
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.default :charset => "utf-8"
ActiveRecord::Base.include_root_in_json = false
ActionMailer::Base.smtp_settings = {
	:address => "smtp.gmail.com",
	:port => 587,
	:domain => "gmail.com",
	:authentication => :login,
	:user_name => "info@gurujiforpuja.com",
	:password => "guruji123",
}
