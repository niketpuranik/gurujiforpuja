Gurujipooja::Application.routes.draw do

  resources :states

  resources :countries

  resources :videos

	match "/booking/new_booking", :to => "booking#new_booking", :as => :new_booking
	match "/booking/create_booking", :to => "booking#create_booking", :as => :create_booking
	match "/booking/update_booking", :to => "booking#update_booking", :as => :update_booking
  match "/import_cities", :to => "cities#import_cities"

	devise_scope :user do
		get '/users/auth/:provider' => 'users/omniauth_callbacks#passthru'
	end
	devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }	

  resources :plans

  resources :locations 

  resources :cities

  resources :pujas

  resources :contacts

	match "/enable_user", :to => "admin#approve_user", :as => :enable_user
	match "/search", :to => "site#search", :as => :search

  devise_for :users
	
	match "/get_daily_events", :to => "daily_events#get_daily_events"

  resources :daily_events
  
 	match "/services", :to => "services#numerology"
	match "/puja_vidhi", :to => "site#pooja_vidhi", :as => :pooja_vidhi
  match "/calendar", :to => "site#calendar", :as => :calendar	
  match "/about_us", :to => "site#about_us", :as => :about_us		
  match "/feedback", :to => "site#feedback", :as => :feedback	
  match "/faqs", :to => "site#faq", :as => :faq		
  match "/contact-us", :to => "site#contact_us", :as => :contact_us	
  match "/guruji/:id", :to => "site#guruji"
  match "/terms_and_conditions", :to => "site#terms", :as => :terms
  match "/gurujiforpuja_site_map", :to => "site#site_map", :as => :site_map
  match "/gurujiforpuja_privacy_policy", :to => "site#privacy_policy", :as => :privacy_policy
  match "/site/plans", :to => "site#plan"
  
  match '/assets/crop', :to => 'assets#crop'
	match '/assets/crop_image', :to => 'assets#crop_image'
	match "/assets/add_lookbook",:to => "assets#add_lookbook"
	match '/assets/variant_assets', :to => 'assets#variant_assets'
	match '/assets/add_to_variants', :to => 'assets#add_to_variants'
	match '/assets/upload_assets', :to => 'assets#upload_assets'
  match '/assets/delete_assets', :to => 'assets#delete_assets'
  match '/assets/create_video', :to => 'assets#create_video'
  match '/assets/new_video', :to => 'assets#new_video'
  match '/assets/video', :to => 'assets#video'
  match '/assets/video_status', :to => 'assets#video_status'
  match '/assets/logo', :to => 'assets#logo'
  match '/assets/add', :to => 'assets#add'
  match '/assets/:id/destroy', :to => 'assets#destroy'
  match '/assets/add_logo', :to => 'assets#add_logo'
  match '/assets/show_update_asset', :to => 'assets#show_update_asset'
  match '/assets/reorder', :to => 'assets#reorder'
  match '/assets/reorder_all', :to => 'assets#reorder_all'
	match '/assets/update', :to => 'assets#update'
	match '/assets/:id', :to => 'assets#show'
	match "/download_asset/:id", :to => "assets#download_asset", :as => :download_asset
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
   root :to => 'site#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
   match ':controller(/:action(/:id))(.:format)'
   
   match "/:guruji_id", :to => "site#guruji", :as => :guruji_details
end
