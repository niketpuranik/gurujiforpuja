class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :name
      t.string :url
      t.text :description
      t.text :tags
      t.integer :user_id

      t.timestamps
    end
  end
end
