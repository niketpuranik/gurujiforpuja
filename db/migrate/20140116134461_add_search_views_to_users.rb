class AddSearchViewsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :search_views, :integer
    add_column :users, :profile_views, :integer
  end
end
