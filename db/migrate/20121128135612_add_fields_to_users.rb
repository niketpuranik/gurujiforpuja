class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :experience, :string
    add_column :users, :specialization, :text
    add_column :users, :offered_pujas, :text
    add_column :users, :terms, :text
    add_column :users, :locations, :text
    add_column :users, :phone, :string
  end
end
