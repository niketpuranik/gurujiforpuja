class CreateDailyTrends < ActiveRecord::Migration
  def change
    create_table :daily_trends do |t|
      t.integer :guruji_id
      t.integer :svc
      t.integer :pvc

      t.timestamps
    end
  end
end
