class AddVisitedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :visited, :boolean
  end
end
