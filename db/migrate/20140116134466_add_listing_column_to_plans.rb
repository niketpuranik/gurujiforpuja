class AddListingColumnToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :listing, :boolean
  end
end
