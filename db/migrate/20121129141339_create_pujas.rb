class CreatePujas < ActiveRecord::Migration
  def change
    create_table :pujas do |t|
      t.string :name
      t.text :description
      t.text :samugri
      t.text :process
      t.string :category

      t.timestamps
    end
  end
end
