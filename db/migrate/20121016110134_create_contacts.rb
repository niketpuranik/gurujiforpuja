class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :fname
      t.string :lname
      t.integer :phone
      t.string :email
      t.text :comment

      t.timestamps
    end
  end
end
