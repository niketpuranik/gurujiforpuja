class AddOfferedServicesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :offered_services, :text
    add_column :users, :cities, :string
  end
end
