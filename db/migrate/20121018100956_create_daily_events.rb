class CreateDailyEvents < ActiveRecord::Migration
  def change
    create_table :daily_events do |t|
      t.string :name
      t.text :description
      t.datetime :from_time
      t.datetime :to_time
      t.string :entity_type
      t.integer :entity_id
      t.text :tags

      t.timestamps
    end
  end
end
