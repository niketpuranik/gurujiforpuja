class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.text :address
      t.integer :puja_id
      t.integer :city_id
      t.integer :location_id
      t.integer :user_id
      t.integer :guruji_id
      t.datetime :prefered_time
      t.text :note

      t.timestamps
    end
  end
end
