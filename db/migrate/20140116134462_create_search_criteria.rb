class CreateSearchCriteria < ActiveRecord::Migration
  def change
    create_table :search_criteria do |t|
      t.integer :puja_id
      t.integer :location_id
      t.integer :city_id
      t.string  :guruji_ids
      t.string  :session_id
			t.integer  :user_id
		
      t.timestamps
    end
  end
end
