class AddSlugToUsers < ActiveRecord::Migration
  def change
    add_column :users, :slug, :string
    add_column :pujas, :slug, :string
    add_column :plans, :slug, :string
    add_column :daily_events, :slug, :string
    add_column :locations, :slug, :string
    add_column :cities, :slug, :string
    add_column :assets, :slug, :string
    
    add_index :users, :slug, unique: true
    add_index :pujas, :slug, unique: true
    add_index :plans, :slug, unique: true
    add_index :daily_events, :slug, unique: true
    add_index :locations, :slug, unique: true
    add_index :cities, :slug, unique: true
    add_index :assets, :slug, unique: true
  end
end
