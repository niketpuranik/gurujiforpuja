class AddPriorityColumnToUser < ActiveRecord::Migration
  def change
    add_column :users, :priority, :int
  end
end
