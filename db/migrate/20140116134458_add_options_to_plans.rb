class AddOptionsToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :options, :text
  end
end
