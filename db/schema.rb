# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140116134471) do

  create_table "assets", :force => true do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "attachment_content_type"
    t.string   "attachment_file_name"
    t.integer  "attachment_size"
    t.integer  "position"
    t.string   "type"
    t.datetime "attachment_updated_at"
    t.integer  "attachment_width"
    t.integer  "attachment_height"
    t.string   "group_id"
    t.integer  "asset_order"
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "slug"
  end

  add_index "assets", ["slug"], :name => "index_assets_on_slug", :unique => true

  create_table "bookings", :force => true do |t|
    t.text     "address"
    t.integer  "puja_id"
    t.integer  "city_id"
    t.integer  "location_id"
    t.integer  "user_id"
    t.integer  "guruji_id"
    t.datetime "prefered_time"
    t.text     "note"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
    t.integer  "state_id"
  end

  add_index "cities", ["slug"], :name => "index_cities_on_slug", :unique => true
  add_index "cities", ["state_id"], :name => "index_cities_on_state_id"

  create_table "contacts", :force => true do |t|
    t.string   "fname"
    t.string   "lname"
    t.integer  "phone"
    t.string   "email"
    t.text     "comment"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "contact_type"
  end

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "daily_events", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "from_time"
    t.datetime "to_time"
    t.string   "entity_type"
    t.integer  "entity_id"
    t.text     "tags"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "puja_ids"
    t.string   "slug"
  end

  add_index "daily_events", ["slug"], :name => "index_daily_events_on_slug", :unique => true

  create_table "daily_trends", :force => true do |t|
    t.integer  "guruji_id"
    t.integer  "svc"
    t.integer  "pvc"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "emails", :force => true do |t|
    t.string   "from"
    t.string   "to"
    t.integer  "last_send_attempt", :default => 0
    t.text     "mail"
    t.datetime "created_on"
    t.text     "subject"
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.integer  "city_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  add_index "locations", ["slug"], :name => "index_locations_on_slug", :unique => true

  create_table "plans", :force => true do |t|
    t.string   "name"
    t.decimal  "price",       :precision => 10, :scale => 0
    t.integer  "duration"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "slug"
    t.text     "options"
    t.boolean  "listing"
  end

  add_index "plans", ["slug"], :name => "index_plans_on_slug", :unique => true

  create_table "pujas", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "samugri"
    t.text     "process"
    t.string   "category"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
  end

  add_index "pujas", ["slug"], :name => "index_pujas_on_slug", :unique => true

  create_table "search_criteria", :force => true do |t|
    t.integer  "puja_id"
    t.integer  "location_id"
    t.integer  "city_id"
    t.string   "guruji_ids"
    t.string   "session_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "states", ["country_id"], :name => "index_states_on_country_id"

  create_table "subscriptions", :force => true do |t|
    t.integer  "plan_id"
    t.integer  "user_id"
    t.date     "start_date"
    t.string   "status"
    t.integer  "repeat_plan"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "user_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "token"
    t.string   "token_secret"
    t.boolean  "approved"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "type"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "dob"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "experience"
    t.text     "specialization"
    t.text     "offered_pujas"
    t.text     "terms"
    t.text     "locations"
    t.string   "phone"
    t.boolean  "approved"
    t.text     "description"
    t.string   "slug"
    t.text     "offered_services"
    t.string   "cities"
    t.integer  "search_views"
    t.integer  "profile_views"
    t.boolean  "visited"
    t.integer  "priority"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["slug"], :name => "index_users_on_slug", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.text     "tags"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
