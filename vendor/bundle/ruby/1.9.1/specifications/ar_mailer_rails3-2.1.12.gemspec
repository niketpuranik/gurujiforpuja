# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "ar_mailer_rails3"
  s.version = "2.1.12"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Yuanyi Zhang"]
  s.date = "2011-03-24"
  s.description = "ArMailer wrapper for Rails 3"
  s.email = "zhangyuanyi@gmail.com"
  s.executables = ["ar_sendmail_rails3"]
  s.extra_rdoc_files = ["LICENSE.txt", "README.rdoc"]
  s.files = ["bin/ar_sendmail_rails3", "LICENSE.txt", "README.rdoc"]
  s.homepage = "http://github.com/yzhang/ar_mailer_rails3"
  s.rdoc_options = ["--charset=UTF-8"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.24"
  s.summary = "ArMailer wrapper for Rails 3"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
