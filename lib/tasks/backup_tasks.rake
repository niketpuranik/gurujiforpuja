task :backup_data => :environment do
	begin
		Rails.logger.info "Starting database backup....on #{Socket.gethostname}"
		
		delete_day = Time.now - 2.days
	  dd = "#{Rails.root}/files/backups/#{delete_day.day}_#{delete_day.month}_#{delete_day.year}"
	  
	  system("rm -rf #{dd}")
	  
	  d = "#{Rails.root}/files/backups/#{Time.now.day}_#{Time.now.month}_#{Time.now.year}"
	  t = "#{Socket.gethostname}_#{Time.now.hour}_#{Time.now.min}.sql"
	  if not File.directory?(d)
	    Dir.mkdir(d)
	  end
	  system "mysqldump -uroot -prot880 gurujipooja_production > #{d}/#{t}"
	  
	  #files = []
	  #files.push File.open("#{d}/#{t}")
	  #files.each do |file|
	  #  filenames = file.original_filename.split(".")
	  #  filenames = filenames - [filenames.last]
	  #  filename = filenames.join(".")
	  #  if File.file?(filename)
	  #    File.delete(filename)
	  #  end
	  #end
	  #zip = "#{Socket.gethostname}_online_exams_data_#{Time.now.day}_#{Time.now.month}_#{Time.now.year}.zip"
	  #system "zip #{d}/#{zip} -9 -r #{d}/#{t}"
	  Rails.logger.info "Database backup completed....on #{Socket.gethostname}"
	  #RakeTaskMailer.notify("GurujiForPuja :: Database backed up successfully...on #{Socket.gethostname}","").deliver
	rescue Exception => e
    RakeTaskMailer.notify("GurujiForPuja :: Backup database failed...on #{Socket.gethostname}","#{e.message}\n#{e.backtrace.join('\n')}").deliver
	end 
end


task :generate_sitemaps_task  => :environment do
	begin
		#RakeTaskMailer.notify("Generating sitemap at #{Time.now} on #{Socket.gethostname}", "Started the rake task :generate_sitemaps").deliver
		Rake::Task["generate_sitemaps"].invoke('http://www.gurujiforpuja.com',false,false)
	rescue Exception => e
		RakeTaskMailer.notify("Error in generate_sitemaps at #{Time.now} on #{Socket.gethostname}", "#{e.message}\n#{e.backtrace.join('\n')}").deliver
	end
end

task :generate_sitemaps,:domain,:test,:quick_test do |t,args|
	sitemap = File.open("#{Rails.root}/public/common_sitemap.xml","r")
	xml = "<?xml version='1.0' encoding='UTF-8'?><urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>#{sitemap.read}"
	gs = File.open("#{Rails.root}/public/sitemap.xml","w")
	gs.write xml
	urls = []
	domain = args[:domain]
	quick_test = args[:quick_test] 
	if not domain or domain.blank?
		domain = "http://www.gurujiforpuja.com"
	end
	test = args[:test] == "true" ? true : false
	quick_test = args[:quick_test] == "true" ? true : false

	i = 0
	Puja.all.each do |puja|
		loc = "<![CDATA[#{domain}/pujas/#{puja.slug}]]>"
		url = "\n<url>\n<loc>\n#{loc}\n</loc>\n<changefreq>daily</changefreq>\n</url>"
		gs.write url.gsub(/ /,'%20')
		if quick_test
			if i <= 5 
				urls.push loc.gsub(/ /,'%20')
			end
		else
			urls.push loc.gsub(/ /,'%20')
		end
		i = i + 1
	end
	
	Guruji.all.each do |guruji|
		loc = "<![CDATA[#{domain}/guruji_details/#{guruji.slug}]]>"
		url = "\n<url>\n<loc>\n#{loc}\n</loc>\n<changefreq>daily</changefreq>\n</url>"
		gs.write url.gsub(/ /,'%20')
		if quick_test
			if i <= 5 
				urls.push loc.gsub(/ /,'%20')
			end
		else
			urls.push loc.gsub(/ /,'%20')
		end
		i = i + 1
	end
	
	gs.write "\n</urlset>"
	gs.close
	
	if test
		p "******************* Testing Urls #{urls.count} Quick Test #{quick_test} *******************"
		errors = []
		urls.each do |url|
			begin
				status = Timeout::timeout(60) {
					url = URI.parse(url.gsub('<![CDATA[','').gsub(']]>',''))
					p "#{url.path}"
					req = Net::HTTP::Get.new(url.path)
					res = Net::HTTP.start(url.host, url.port) {|http|
						http.request(req)
					}
					p "STATUS :: #{res.code}"
					code = res.code.to_i
					if code >= 200 and code < 300
						p "SUCCESS :: #{url}"
					elsif code >= 300 and code < 400
						p "SUCCESS :: #{url}"
					elsif code >= 400 and code < 500
						p "ERROR #{code} :: #{url}"
						p "#{res}"
						errors.push "Error #{code} :: #{url}"
					elsif code >= 500
						p "ERROR #{code} :: #{url}"
						p "#{res}"
						errors.push "Error #{code} :: #{url}"
					else
						p "ERROR #{code} :: #{url}"
						p "#{res}"
						errors.push "Error #{code} :: #{url}"
					end
				}
			rescue Timeout::Error => e
				p "Timeout::Error ... #{url}"
				errors.push "#{url}:::#{e.message}"
			rescue Exception => e
				p "ERROR :: #{e.message}"
				errors.push "#{url}:::#{e.message}"
			end
		end
		#RakeTaskMailer.notify("Check Urls Completed", "Errors For Urls \n#{errors.join('\n')}").deliver!
	end
end

