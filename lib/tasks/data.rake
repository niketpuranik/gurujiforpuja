require 'csv'
task :dump_data => :environment do
  file_names = [".","..",".svn"]
  Dir.entries("#{Rails.root}/files").each do |file|
  	if not file_names.include?(file)
			CSV.open("#{Rails.root}/files/#{file}","r") do |csv|
				csv.each_with_index do |row,index|
					city_name = row[0].strip.capitalize rescue nil
					location_name = row[1].strip.titleize rescue nil
					puts "Row #{index} : #{city_name} - #{location_name}"
					city = City.where(:name => city_name).first
					if not city
						city = City.new(:name => city_name)
						city.save!
						puts "City Created..."
					else
					end
					location = Location.where(:name => location_name).first
					if not location
						location = Location.new(:city_id => city.id, :name => location_name)
						location.save!
						puts "Location Created..."
					end
				end
			end
  	end
  end
end
